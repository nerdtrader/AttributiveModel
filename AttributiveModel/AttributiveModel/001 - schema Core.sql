use AM
go
if not exists
(
	select
		*
	from
		sys.schemas s
	where
		s.name = 'Core'
)
begin
	exec ('create schema Core')
end
go