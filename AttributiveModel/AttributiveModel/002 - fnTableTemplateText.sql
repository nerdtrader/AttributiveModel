use AM
go
if object_id('Core.fnTableTemplateText') is null
	exec('create function Core.fnTableTemplateText() returns varchar(max) as begin return null end')
go
alter function Core.fnTableTemplateText() returns varchar(max) 
as 
begin
	return
		'
		go
		if object_id(''%SchemaName%.%TableName%'') is not null
			drop table %SchemaName%.%TableName%
		go
		create table %SchemaName%.%TableName%
		(
			ID              bigint       constraint PK__%SchemaName%_%TableName%__ID              primary key clustered
		,	StartUpDate     datetime     constraint DF__%SchemaName%_%TableName%__StartUpDate     default(getdate())
		,	CreateDate      datetime     constraint DF__%SchemaName%_%TableName%__CreateDate      default(getdate())
		,	CreateUser      varchar(255) constraint DF__%SchemaName%_%TableName%__CreateUser      default(suser_name())
		,	CreateHost	    varchar(255) constraint DF__%SchemaName%_%TableName%__CreateHost      default(host_name())
		,	CreateApp       varchar(255) constraint DF__%SchemaName%_%TableName%__CreateApp       default(app_name())
		,	CreateProcedure varchar(255) constraint DF__%SchemaName%_%TableName%__CreateProcedure default(object_name(@@procid))
		,	DeleteDate      datetime
		,	DeleteUser      varchar(255)
		,	DeleteHost	    varchar(255)
		,	DeleteApp       varchar(255)
		,	DeleteProcedure varchar(255)
		,	RV              rowversion   not null
		)	
		go
		-- �������� ��������
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@value        = N''%TableDefinition%''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''ID''
		,	@value        = N''������������� �������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''StartUpDate''
		,	@value        = N''���� ���������� ��������� � ����''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''CreateDate''
		,	@value        = N''�����������. ���� �������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''CreateUser''
		,	@value        = N''�����������. ��� ������������, ���������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''CreateHost''
		,	@value        = N''�����������. ��������� ������������, ���������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''CreateApp''
		,	@value        = N''�����������. ���������� ������������, ���������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''CreateProcedure''
		,	@value        = N''�����������. ���������, ��������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''DeleteDate''
		,	@value        = N''�����������. ���� �������� ������. ����� - ������ �� �������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''DeleteUser''
		,	@value        = N''�����������. ��� ������������, ���������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''DeleteHost''
		,	@value        = N''�����������. ��������� ������������, ���������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''DeleteApp''
		,	@value        = N''�����������. ���������� ������������, ���������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''DeleteProcedure''
		,	@value        = N''�����������. ���������, ��������� ������''
		go
		exec sys.sp_addextendedproperty
			@name         = N''MS_Description''
		,	@level0type   = ''SCHEMA''
		,	@level0name   = N''%SchemaName%''
		,	@level1type   = ''TABLE''
		,	@level1name   = N''%TableName%''
		,	@level2type   = ''COLUMN''
		,	@level2name   = N''RV''
		,	@value        = N''�����������. ������ ������''
		go
		'
end
go