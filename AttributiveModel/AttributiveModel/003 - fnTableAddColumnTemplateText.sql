use AM
go
if object_id('Core.fnTableAddColumnTemplateText') is null
	exec('create function Core.fnTableAddColumnTemplateText() returns varchar(max) as begin return null end')
go
alter function Core.fnTableAddColumnTemplateText() returns varchar(max) 
as 
begin
	return '
if columnproperty(object_id(''%SchemaName%.%TableName%''),''%ColumnName%'',''AllowsNull'') is not null
begin
	alter table %SchemaName%.%TableName% alter column %ColumnName% varchar(max)
	exec sys.sp_updateextendedproperty
		@name         = N''MS_Description''
	,	@level0type   = ''SCHEMA''
	,	@level0name   = N''%SchemaName%''
	,	@level1type   = ''TABLE''
	,	@level1name   = N''%TableName%''
	,	@level2type   = ''COLUMN''
	,	@level2name   = N''%ColumnName%''
	,	@value        = N''%ColumnDescription%''
end
else
begin 
	alter table %SchemaName%.%TableName% add %ColumnName% varchar(max)
	exec sys.sp_addextendedproperty
		@name         = N''MS_Description''
	,	@level0type   = ''SCHEMA''
	,	@level0name   = N''%SchemaName%''
	,	@level1type   = ''TABLE''
	,	@level1name   = N''%TableName%''
	,	@level2type   = ''COLUMN''
	,	@level2name   = N''%ColumnName%''
	,	@value        = N''%ColumnDescription%''
end
go
'
end
go