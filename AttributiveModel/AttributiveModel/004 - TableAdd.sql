use AM
go
if object_id('Core.TableAdd') is null
	exec('create procedure Core.TableAdd as begin return end')
go
alter procedure Core.TableAdd
	@SchemaName      varchar(255)
,	@TableName       varchar(255)
,	@TableDefinition varchar(255)
as
begin
	declare
		@TableTemplate varchar(max)

	select @TableTemplate = Core.fnTableTemplateText();

	select @TableTemplate = replace(@TableTemplate, '%SchemaName%', @SchemaName)
	select @TableTemplate = replace(@TableTemplate, '%TableName%', @TableName)
	select @TableTemplate = replace(@TableTemplate, '%TableDefinition%', @TableDefinition)

	exec (@TableTemplate)
end