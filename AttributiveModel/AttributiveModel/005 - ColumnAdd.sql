use AM
go
if object_id('Core.ColumnAdd') is null
	exec('create procedure Core.ColumnAdd as begin return end')
go
alter procedure Core.ColumnAdd
	@SchemaName        varchar(255)
,	@TableName         varchar(255)
,	@ColumnName        varchar(255)
,	@ColumnDescription varchar(255)
as
begin
	declare
		@ColumnTemplate varchar(max)

	select @ColumnTemplate = Core.fnTableAddColumnTemplateText();

	select @ColumnTemplate = replace(@ColumnTemplate, '%SchemaName%', @SchemaName)
	select @ColumnTemplate = replace(@ColumnTemplate, '%TableName%', @TableName)
	select @ColumnTemplate = replace(@ColumnTemplate, '%ColumnName%', @ColumnName)
	select @ColumnTemplate = replace(@ColumnTemplate, '%ColumnDescription%', @ColumnDescription)

	exec (@ColumnTemplate)
end